package ru.dileveryClub.invaildFindRequest;

import io.qameta.allure.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FindCityPage {

    private WebDriver driver;

    public FindCityPage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id=\"user-addr__input\"]")
    private WebElement input;
    @FindBy(xpath = "//*[@id=\"user-addr__form\"]/label[1]/span[2]/span[3]")
    private WebElement answerError;

    private By buttonlocator = By.xpath("//*[@id=\"user-addr__form\"]/label[2]/a");

    @Step("Открываем страницу Delivery-club")
    public void open(){
        driver.get("https://www.delivery-club.ru/");
    }

    @Step("Вводим неккоректное название города")
    public void getStart(){
        input.sendKeys("Мпапаввыс");
        WebElement FindButton = driver.findElement(buttonlocator);
        FindButton.click();
    }


    public void inccorrectAnswer(){
      try {
          driver.findElement(By.xpath("//*[@id=\"user-addr__form\"]/label[1]/span[2]/span[3]"));
      } catch (NoSuchWindowException e){
          System.out.println(false);
      }
        System.out.println(true);
    }

    public void end(){
        driver.quit();
    }
}