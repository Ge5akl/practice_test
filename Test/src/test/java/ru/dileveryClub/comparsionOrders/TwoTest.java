package ru.dileveryClub.comparsionOrders;

import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import ru.dileveryClub.WebDriverSettings;

public class TwoTest extends WebDriverSettings {

    @Test
    public void ComparsionOrder() throws InterruptedException {

        FindCityPage findCityPage = PageFactory.initElements(driver, FindCityPage.class);

        findCityPage.open();
        findCityPage.getStart();

        CityShopPage cityShopPage = PageFactory.initElements(driver,CityShopPage.class);

        cityShopPage.clickCheckBoxMenu();
        cityShopPage.findShop();
        cityShopPage.clickFindedShop();
        cityShopPage.createOrder();
        cityShopPage.orderBasket();
        cityShopPage.endOrder();

    }
}
