package ru.dileveryClub.createOrder;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CityShopPage {

    private WebDriver driver;

    public CityShopPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id=\"vendor-list-app\"]/div/div[1]/div[1]/form/section[1]/div[2]/div[1]/label[1]/span")
    private WebElement vendorListApp;
    @FindBy(xpath = "//*[@id=\"vendor-list-app\"]/div/div[2]/div[1]/form/div/label/input")
    private WebElement findShop;
    @FindBy(xpath = "//*[@id=\"vendor-list-app\"]/div/div[2]/div[6]/ul/li[1]/section/div[2]/section[1]/a")
    private WebElement buttonShop;
    @FindBy (xpath = "//*[@id=\"content\"]/ul[1]/li[1]/form/p/i")
    private WebElement elementOrderButton;
    @FindBy (xpath = "//*[@id=\"quantity\"]/div[1]/a[2]")
    private WebElement incrementOrderButton;
    @FindBy (xpath = "//*[@id=\"quantity\"]/div[1]/a[3]")
    private WebElement apllyButton;
    @FindBy (xpath = "//*[@id=\"content\"]/ul[1]/li[2]/form/p/a")
    private WebElement orderBox;
    @FindBy (xpath = "//*[@id=\"middle\"]/div[3]/a[2]")
    private WebElement basketButton;
    @FindBy (xpath = "//*[@id=\"popup\"]/div[2]/h2/a")
    private WebElement applyBasketButton;



    @Step("Нажимаем на нужный пункт Чекбокса")
    public void clickCheckBoxMenu() {

        driver.navigate().to("https://izhevsk.delivery-club.ru/entities/food/#all");
        vendorListApp.click();
    }

    @Step("Вводим в поле наименование магазина")
    public void findShop() throws InterruptedException {

        findShop.sendKeys("SUSHI LOVE");
        Thread.sleep(2000);
        findShop.sendKeys(Keys.DOWN);
        Thread.sleep(2000);
        findShop.sendKeys(Keys.ENTER);
        Thread.sleep(2000);

    }

    @Step("Нажимаем на найденый магазин")
    public void clickFindedShop(){
        buttonShop.click();
    }

    @Step("Создаем заказ из некоторых элементов")
    public void createOrder() throws InterruptedException {

        elementOrderButton.click();
        incrementOrderButton.click();
        incrementOrderButton.click();
        incrementOrderButton.click();
        incrementOrderButton.click();
        incrementOrderButton.sendKeys(Keys.DOWN);
        incrementOrderButton.sendKeys(Keys.DOWN);
        Thread.sleep(2000);
        apllyButton.click();
        orderBox.click();

    }

    @Step("Нажиммаем на кнопку 'Корзинка'")
    public void orderBasket() throws InterruptedException {

        basketButton.click();
        Thread.sleep(2000);
        applyBasketButton.click();
        Thread.sleep(2000);
    }

    @Step("Завершаем заказ")
    public void endOrder(){
        driver.quit();
    }

}
