package ru.dileveryClub.createOrder;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FindCityPage {

    private WebDriver driver;

    public FindCityPage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id=\"user-addr__input\"]")
    private WebElement input;

    private By buttonlocator = By.xpath("//*[@id=\"user-addr__form\"]/label[2]/a");

    @Step("Открываем страницу Delivery-club")
    public void open(){
         driver.get("https://www.delivery-club.ru/");
     }

    @Step("Вводим в поле нужный город")
     public void getStart(){
         input.sendKeys("Ижевск, Пушкинская, 23");
         WebElement FindButton = driver.findElement(buttonlocator);
         FindButton.click();
     }
}
